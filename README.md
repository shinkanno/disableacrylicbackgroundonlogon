---
title: DisableAcrylicBackgroundOnLogon
description: Windows 10 1903 で追加された、サインイン画面がぼやける機能を無効／有効化するレジストリパッチ
author: Shinji KANNO <shin.kanno@gmail.com>
categories: [windows, batch, registry, patch]
---

# DisableAcrylicBackgroundOnLogon

Windows 10 1903 で追加された、サインイン画面がぼやける機能を無効／有効化するレジストリパッチです。

| ファイル名                          | 説明                                  |
| ----------------------------------- | ------------------------------------- |
| DisableAcrylicBackgroundOnLogon.bat | ぼかしを無効化するバッチファイル      |
| DisableAcrylicBackgroundOnLogon.reg | ぼかしを無効化するレジストリファイル  |
| EnableAcrylicBackgroundOnLogon.bat  | ぼかしを有効化するバッチファイル      |
| EnableAcrylicBackgroundOnLogon.reg  | ぼかしを有効化するレジストリファイル  |

**※レジストリを更新しますので無保証です。**
